To clone
```bash
git clone --recurse-submodules https://gitlab.com/rcbenchmark/rcbenchmark-proto_files
```

Repository that contains all the proto files used at RCBenchmark.
A docker image and a script are also provided to generate files from them.
To update them run the command inside this repo:
```bash
docker run -v $(pwd):/mnt befuhro/rcb-proto-gen
```
or this one for windows 10 (repo must be cloned using WSL and not gitbash)
```bash
docker run -v ${pwd}:/mnt befuhro/rcb-proto-gen

```

To rebuild the Dockerfile
```bash
docker build -t rcb-proto-gen .

```
