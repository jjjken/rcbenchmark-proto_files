// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var notifier_pb = require('./notifier_pb.js');
var parameter_pb = require('./parameter_pb.js');

function serialize_notifierproto_Empty(arg) {
  if (!(arg instanceof notifier_pb.Empty)) {
    throw new Error('Expected argument of type notifierproto.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_notifierproto_Empty(buffer_arg) {
  return notifier_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_parameter_Parameter(arg) {
  if (!(arg instanceof parameter_pb.Parameter)) {
    throw new Error('Expected argument of type parameter.Parameter');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_parameter_Parameter(buffer_arg) {
  return parameter_pb.Parameter.deserializeBinary(new Uint8Array(buffer_arg));
}


var NotifierService = exports.NotifierService = {
  subscribeToParameters: {
    path: '/notifierproto.Notifier/SubscribeToParameters',
    requestStream: false,
    responseStream: true,
    requestType: notifier_pb.Empty,
    responseType: parameter_pb.Parameter,
    requestSerialize: serialize_notifierproto_Empty,
    requestDeserialize: deserialize_notifierproto_Empty,
    responseSerialize: serialize_parameter_Parameter,
    responseDeserialize: deserialize_parameter_Parameter,
  },
};

exports.NotifierClient = grpc.makeGenericClientConstructor(NotifierService);
