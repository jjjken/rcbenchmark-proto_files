// GENERATED CODE -- DO NOT EDIT!

'use strict';
var logger_pb = require('./logger_pb.js');

function serialize_logger_Benchmark(arg) {
  if (!(arg instanceof logger_pb.Benchmark)) {
    throw new Error('Expected argument of type logger.Benchmark');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_logger_Benchmark(buffer_arg) {
  return logger_pb.Benchmark.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_logger_Benchmarks(arg) {
  if (!(arg instanceof logger_pb.Benchmarks)) {
    throw new Error('Expected argument of type logger.Benchmarks');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_logger_Benchmarks(buffer_arg) {
  return logger_pb.Benchmarks.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_logger_CSVInfo(arg) {
  if (!(arg instanceof logger_pb.CSVInfo)) {
    throw new Error('Expected argument of type logger.CSVInfo');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_logger_CSVInfo(buffer_arg) {
  return logger_pb.CSVInfo.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_logger_Empty(arg) {
  if (!(arg instanceof logger_pb.Empty)) {
    throw new Error('Expected argument of type logger.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_logger_Empty(buffer_arg) {
  return logger_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_logger_LoggerInformation(arg) {
  if (!(arg instanceof logger_pb.LoggerInformation)) {
    throw new Error('Expected argument of type logger.LoggerInformation');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_logger_LoggerInformation(buffer_arg) {
  return logger_pb.LoggerInformation.deserializeBinary(new Uint8Array(buffer_arg));
}


var LoggerService = exports['logger.Logger'] = {
  // Keep clients up to date with logger information
  handleLoggerInformation: {
    path: '/logger.Logger/HandleLoggerInformation',
    requestStream: true,
    responseStream: true,
    requestType: logger_pb.LoggerInformation,
    responseType: logger_pb.LoggerInformation,
    requestSerialize: serialize_logger_LoggerInformation,
    requestDeserialize: deserialize_logger_LoggerInformation,
    responseSerialize: serialize_logger_LoggerInformation,
    responseDeserialize: deserialize_logger_LoggerInformation,
  },
  // Generate CSV file inside the directory
  generateCSV: {
    path: '/logger.Logger/GenerateCSV',
    requestStream: false,
    responseStream: false,
    requestType: logger_pb.CSVInfo,
    responseType: logger_pb.Empty,
    requestSerialize: serialize_logger_CSVInfo,
    requestDeserialize: deserialize_logger_CSVInfo,
    responseSerialize: serialize_logger_Empty,
    responseDeserialize: deserialize_logger_Empty,
  },
  // Take sample at t time
  takeSample: {
    path: '/logger.Logger/TakeSample',
    requestStream: false,
    responseStream: false,
    requestType: logger_pb.Empty,
    responseType: logger_pb.Empty,
    requestSerialize: serialize_logger_Empty,
    requestDeserialize: deserialize_logger_Empty,
    responseSerialize: serialize_logger_Empty,
    responseDeserialize: deserialize_logger_Empty,
  },
  // Handle meta data of previous benchmarks
  benchmarksInformation: {
    path: '/logger.Logger/BenchmarksInformation',
    requestStream: true,
    responseStream: true,
    requestType: logger_pb.Benchmarks,
    responseType: logger_pb.Benchmarks,
    requestSerialize: serialize_logger_Benchmarks,
    requestDeserialize: deserialize_logger_Benchmarks,
    responseSerialize: serialize_logger_Benchmarks,
    responseDeserialize: deserialize_logger_Benchmarks,
  },
  // Delete benchmark
  deleteBenchmark: {
    path: '/logger.Logger/DeleteBenchmark',
    requestStream: false,
    responseStream: false,
    requestType: logger_pb.Benchmark,
    responseType: logger_pb.Empty,
    requestSerialize: serialize_logger_Benchmark,
    requestDeserialize: deserialize_logger_Benchmark,
    responseSerialize: serialize_logger_Empty,
    responseDeserialize: deserialize_logger_Empty,
  },
};

