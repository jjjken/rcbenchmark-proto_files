// Code generated by protoc-gen-go. DO NOT EDIT.
// source: firmware.proto

package firmwareproto

import (
        . "gitlab.com/rcbenchmark/rcbenchmark-proto_files/golang/error"
        . "gitlab.com/rcbenchmark/rcbenchmark-proto_files/golang/handshake"
        . "gitlab.com/rcbenchmark/rcbenchmark-proto_files/golang/parameter"
        . "gitlab.com/rcbenchmark/rcbenchmark-proto_files/golang/sensor_data"
        
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type FirmwareToCore struct {
	BoardID              uint32        `protobuf:"varint,1,opt,name=BoardID,proto3" json:"BoardID,omitempty"`
	HandShake            []*HandShake  `protobuf:"bytes,2,rep,name=HandShake,proto3" json:"HandShake,omitempty"`
	Parameters           []*Parameter  `protobuf:"bytes,3,rep,name=Parameters,proto3" json:"Parameters,omitempty"`
	Data                 []*SensorData `protobuf:"bytes,4,rep,name=Data,proto3" json:"Data,omitempty"`
	Error                []*Error      `protobuf:"bytes,5,rep,name=Error,proto3" json:"Error,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *FirmwareToCore) Reset()         { *m = FirmwareToCore{} }
func (m *FirmwareToCore) String() string { return proto.CompactTextString(m) }
func (*FirmwareToCore) ProtoMessage()    {}
func (*FirmwareToCore) Descriptor() ([]byte, []int) {
	return fileDescriptor_138455e383c002dd, []int{0}
}

func (m *FirmwareToCore) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FirmwareToCore.Unmarshal(m, b)
}
func (m *FirmwareToCore) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FirmwareToCore.Marshal(b, m, deterministic)
}
func (m *FirmwareToCore) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FirmwareToCore.Merge(m, src)
}
func (m *FirmwareToCore) XXX_Size() int {
	return xxx_messageInfo_FirmwareToCore.Size(m)
}
func (m *FirmwareToCore) XXX_DiscardUnknown() {
	xxx_messageInfo_FirmwareToCore.DiscardUnknown(m)
}

var xxx_messageInfo_FirmwareToCore proto.InternalMessageInfo

func (m *FirmwareToCore) GetBoardID() uint32 {
	if m != nil {
		return m.BoardID
	}
	return 0
}

func (m *FirmwareToCore) GetHandShake() []*HandShake {
	if m != nil {
		return m.HandShake
	}
	return nil
}

func (m *FirmwareToCore) GetParameters() []*Parameter {
	if m != nil {
		return m.Parameters
	}
	return nil
}

func (m *FirmwareToCore) GetData() []*SensorData {
	if m != nil {
		return m.Data
	}
	return nil
}

func (m *FirmwareToCore) GetError() []*Error {
	if m != nil {
		return m.Error
	}
	return nil
}

func init() {
	proto.RegisterType((*FirmwareToCore)(nil), "firmwareproto.FirmwareToCore")
}

func init() { proto.RegisterFile("firmware.proto", fileDescriptor_138455e383c002dd) }

var fileDescriptor_138455e383c002dd = []byte{
	// 220 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x4c, 0x8f, 0xcf, 0x4e, 0x84, 0x30,
	0x10, 0xc6, 0x83, 0xbb, 0xab, 0x71, 0xd6, 0x5d, 0xb4, 0xf1, 0xd0, 0x70, 0x22, 0xc4, 0x03, 0xa7,
	0x1e, 0xd0, 0x27, 0x50, 0x34, 0x7a, 0x33, 0xc5, 0xbb, 0x19, 0xd3, 0x1a, 0x8c, 0x81, 0x92, 0x69,
	0x13, 0xdf, 0xd6, 0x67, 0x31, 0x0c, 0x05, 0x3c, 0x7e, 0x7f, 0xe6, 0xcb, 0xfc, 0xe0, 0xf8, 0xf9,
	0x45, 0xdd, 0x0f, 0x92, 0x55, 0x03, 0xb9, 0xe0, 0xc4, 0x61, 0xd6, 0x2c, 0xb3, 0x74, 0x40, 0xc2,
	0xce, 0x06, 0x4b, 0x53, 0x9e, 0x5d, 0x79, 0xdb, 0x7b, 0x47, 0xef, 0x06, 0x03, 0x46, 0x2b, 0x6d,
	0xb1, 0x37, 0xbe, 0xc5, 0xef, 0xb8, 0x91, 0xed, 0x2d, 0x91, 0x8b, 0x07, 0xc5, 0x6f, 0x02, 0xc7,
	0xa7, 0xb8, 0xf9, 0xe6, 0x1e, 0x1c, 0x59, 0x21, 0xe1, 0xec, 0xde, 0x21, 0x99, 0x97, 0x5a, 0x26,
	0x79, 0x52, 0x1e, 0xf4, 0x2c, 0x45, 0x05, 0xe7, 0xcf, 0xd8, 0x9b, 0x66, 0x1c, 0x93, 0x27, 0xf9,
	0xa6, 0xdc, 0x57, 0xd7, 0x6a, 0x9d, 0x5f, 0x32, 0xbd, 0xd6, 0xc4, 0x1d, 0xc0, 0xeb, 0xfc, 0xa4,
	0x97, 0x9b, 0x78, 0xb4, 0xfe, 0xbd, 0x84, 0xfa, 0x5f, 0x4f, 0xdc, 0xc0, 0xb6, 0xc6, 0x80, 0x72,
	0xcb, 0xfd, 0x4b, 0xc5, 0x3c, 0x0d, 0xb3, 0x8d, 0xbe, 0xe6, 0x54, 0x14, 0xb0, 0x7b, 0x1c, 0x59,
	0xe4, 0x8e, 0x6b, 0x17, 0x6a, 0x22, 0x63, 0x4f, 0x4f, 0xd1, 0xc7, 0x29, 0x73, 0xde, 0xfe, 0x05,
	0x00, 0x00, 0xff, 0xff, 0x1e, 0x05, 0xe4, 0x20, 0x4a, 0x01, 0x00, 0x00,
}
