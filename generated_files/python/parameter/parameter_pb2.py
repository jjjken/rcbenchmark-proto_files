# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: parameter.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='parameter.proto',
  package='parameter',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x0fparameter.proto\x12\tparameter\"\x90\x03\n\tParameter\x12\x10\n\x08SensorID\x18\x01 \x01(\r\x12\x0c\n\x04name\x18\x02 \x01(\t\x12\x0c\n\x04help\x18\x03 \x01(\t\x12\x36\n\x0bstring_type\x18\x05 \x01(\x0b\x32\x1f.parameter.Parameter.StringTypeH\x00\x12\x36\n\x0buint32_type\x18\x06 \x01(\x0b\x32\x1f.parameter.Parameter.Uint32TypeH\x00\x12\x34\n\nfloat_type\x18\x07 \x01(\x0b\x32\x1e.parameter.Parameter.FloatTypeH\x00\x1a,\n\nStringType\x12\r\n\x05value\x18\x01 \x01(\t\x12\x0f\n\x07\x64\x65\x66\x61ult\x18\x02 \x01(\t\x1a,\n\nUint32Type\x12\r\n\x05value\x18\x01 \x01(\r\x12\x0f\n\x07\x64\x65\x66\x61ult\x18\x02 \x01(\r\x1a\x45\n\tFloatType\x12\r\n\x05value\x18\x01 \x01(\x02\x12\x0f\n\x07\x64\x65\x66\x61ult\x18\x02 \x01(\x02\x12\x0b\n\x03min\x18\x03 \x01(\x02\x12\x0b\n\x03max\x18\x04 \x01(\x02\x42\x0c\n\nSubMessageb\x06proto3')
)




_PARAMETER_STRINGTYPE = _descriptor.Descriptor(
  name='StringType',
  full_name='parameter.Parameter.StringType',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='value', full_name='parameter.Parameter.StringType.value', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='default', full_name='parameter.Parameter.StringType.default', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=256,
  serialized_end=300,
)

_PARAMETER_UINT32TYPE = _descriptor.Descriptor(
  name='Uint32Type',
  full_name='parameter.Parameter.Uint32Type',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='value', full_name='parameter.Parameter.Uint32Type.value', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='default', full_name='parameter.Parameter.Uint32Type.default', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=302,
  serialized_end=346,
)

_PARAMETER_FLOATTYPE = _descriptor.Descriptor(
  name='FloatType',
  full_name='parameter.Parameter.FloatType',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='value', full_name='parameter.Parameter.FloatType.value', index=0,
      number=1, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='default', full_name='parameter.Parameter.FloatType.default', index=1,
      number=2, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='min', full_name='parameter.Parameter.FloatType.min', index=2,
      number=3, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='max', full_name='parameter.Parameter.FloatType.max', index=3,
      number=4, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=348,
  serialized_end=417,
)

_PARAMETER = _descriptor.Descriptor(
  name='Parameter',
  full_name='parameter.Parameter',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='SensorID', full_name='parameter.Parameter.SensorID', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name', full_name='parameter.Parameter.name', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='help', full_name='parameter.Parameter.help', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='string_type', full_name='parameter.Parameter.string_type', index=3,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='uint32_type', full_name='parameter.Parameter.uint32_type', index=4,
      number=6, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='float_type', full_name='parameter.Parameter.float_type', index=5,
      number=7, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[_PARAMETER_STRINGTYPE, _PARAMETER_UINT32TYPE, _PARAMETER_FLOATTYPE, ],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='SubMessage', full_name='parameter.Parameter.SubMessage',
      index=0, containing_type=None, fields=[]),
  ],
  serialized_start=31,
  serialized_end=431,
)

_PARAMETER_STRINGTYPE.containing_type = _PARAMETER
_PARAMETER_UINT32TYPE.containing_type = _PARAMETER
_PARAMETER_FLOATTYPE.containing_type = _PARAMETER
_PARAMETER.fields_by_name['string_type'].message_type = _PARAMETER_STRINGTYPE
_PARAMETER.fields_by_name['uint32_type'].message_type = _PARAMETER_UINT32TYPE
_PARAMETER.fields_by_name['float_type'].message_type = _PARAMETER_FLOATTYPE
_PARAMETER.oneofs_by_name['SubMessage'].fields.append(
  _PARAMETER.fields_by_name['string_type'])
_PARAMETER.fields_by_name['string_type'].containing_oneof = _PARAMETER.oneofs_by_name['SubMessage']
_PARAMETER.oneofs_by_name['SubMessage'].fields.append(
  _PARAMETER.fields_by_name['uint32_type'])
_PARAMETER.fields_by_name['uint32_type'].containing_oneof = _PARAMETER.oneofs_by_name['SubMessage']
_PARAMETER.oneofs_by_name['SubMessage'].fields.append(
  _PARAMETER.fields_by_name['float_type'])
_PARAMETER.fields_by_name['float_type'].containing_oneof = _PARAMETER.oneofs_by_name['SubMessage']
DESCRIPTOR.message_types_by_name['Parameter'] = _PARAMETER
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Parameter = _reflection.GeneratedProtocolMessageType('Parameter', (_message.Message,), {

  'StringType' : _reflection.GeneratedProtocolMessageType('StringType', (_message.Message,), {
    'DESCRIPTOR' : _PARAMETER_STRINGTYPE,
    '__module__' : 'parameter_pb2'
    # @@protoc_insertion_point(class_scope:parameter.Parameter.StringType)
    })
  ,

  'Uint32Type' : _reflection.GeneratedProtocolMessageType('Uint32Type', (_message.Message,), {
    'DESCRIPTOR' : _PARAMETER_UINT32TYPE,
    '__module__' : 'parameter_pb2'
    # @@protoc_insertion_point(class_scope:parameter.Parameter.Uint32Type)
    })
  ,

  'FloatType' : _reflection.GeneratedProtocolMessageType('FloatType', (_message.Message,), {
    'DESCRIPTOR' : _PARAMETER_FLOATTYPE,
    '__module__' : 'parameter_pb2'
    # @@protoc_insertion_point(class_scope:parameter.Parameter.FloatType)
    })
  ,
  'DESCRIPTOR' : _PARAMETER,
  '__module__' : 'parameter_pb2'
  # @@protoc_insertion_point(class_scope:parameter.Parameter)
  })
_sym_db.RegisterMessage(Parameter)
_sym_db.RegisterMessage(Parameter.StringType)
_sym_db.RegisterMessage(Parameter.Uint32Type)
_sym_db.RegisterMessage(Parameter.FloatType)


# @@protoc_insertion_point(module_scope)
