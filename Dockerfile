FROM debian:jessie

# Install all dependencies
RUN echo "\
deb http://deb.debian.org/debian buster main\n\
deb http://deb.debian.org/debian buster-updates main\n\
deb http://security.debian.org buster/updates main\n\
deb http://ftp.de.debian.org/debian testing main\n\
" > /etc/apt/sources.list 
RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y apt-utils ca-certificates
RUN apt-get install -y python3 python3-pip
RUN pip install setuptools -U

RUN apt-get install -y curl software-properties-common
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN npm install --unsafe-perm -g grpc-tools
RUN apt-get install -y wget unzip git gcc make musl-dev python-dev

# Install golang
WORKDIR /opt
RUN	wget https://dl.google.com/go/go1.14.4.linux-amd64.tar.gz
RUN	tar -C /usr/local -xvzf go1.14.4.linux-amd64.tar.gz
RUN	mkdir -p /root/go
ENV	GOROOT=/usr/local/go
ENV	GOPATH=/root/go
ENV	PATH=$PATH:$GOPATH/bin:$GOROOT/bin

# Install protobuf/gRPC and their implementations for go, python and arduino.
WORKDIR	/opt
RUN	wget https://github.com/protocolbuffers/protobuf/releases/download/v3.12.3/protoc-3.12.3-linux-x86_64.zip
RUN	unzip protoc-3.12.3-linux-x86_64.zip -d protoc
ENV 	PATH=$PATH:/opt/protoc/bin
RUN	go get github.com/golang/protobuf/protoc-gen-go
RUN	pip install grpcio-tools
RUN	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
RUN	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
RUN	go get -u github.com/golang/protobuf/protoc-gen-go

# Install nodejs module for grpc-js
RUN npm install -g google-auth-library
RUN npm install -g @grpc/grpc-js

# Mount the current directory and make it the working directory
VOLUME	/mnt
WORKDIR	/mnt

# Run the script that generate files from all the proto files.
CMD	./update.sh
