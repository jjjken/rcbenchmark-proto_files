// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var core_manager_pb = require('./core_manager_pb.js');
var handshake_pb = require('./handshake_pb.js');
var parameter_pb = require('./parameter_pb.js');
var sensor_data_pb = require('./sensor_data_pb.js');

function serialize_coremanager_Benchmark(arg) {
  if (!(arg instanceof core_manager_pb.Benchmark)) {
    throw new Error('Expected argument of type coremanager.Benchmark');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_coremanager_Benchmark(buffer_arg) {
  return core_manager_pb.Benchmark.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_coremanager_CSVInfo(arg) {
  if (!(arg instanceof core_manager_pb.CSVInfo)) {
    throw new Error('Expected argument of type coremanager.CSVInfo');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_coremanager_CSVInfo(buffer_arg) {
  return core_manager_pb.CSVInfo.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_coremanager_CoreChange(arg) {
  if (!(arg instanceof core_manager_pb.CoreChange)) {
    throw new Error('Expected argument of type coremanager.CoreChange');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_coremanager_CoreChange(buffer_arg) {
  return core_manager_pb.CoreChange.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_coremanager_CutoffModifier(arg) {
  if (!(arg instanceof core_manager_pb.CutoffModifier)) {
    throw new Error('Expected argument of type coremanager.CutoffModifier');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_coremanager_CutoffModifier(buffer_arg) {
  return core_manager_pb.CutoffModifier.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_coremanager_Empty(arg) {
  if (!(arg instanceof core_manager_pb.Empty)) {
    throw new Error('Expected argument of type coremanager.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_coremanager_Empty(buffer_arg) {
  return core_manager_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_coremanager_InterpolationLapse(arg) {
  if (!(arg instanceof core_manager_pb.InterpolationLapse)) {
    throw new Error('Expected argument of type coremanager.InterpolationLapse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_coremanager_InterpolationLapse(buffer_arg) {
  return core_manager_pb.InterpolationLapse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_coremanager_Path(arg) {
  if (!(arg instanceof core_manager_pb.Path)) {
    throw new Error('Expected argument of type coremanager.Path');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_coremanager_Path(buffer_arg) {
  return core_manager_pb.Path.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_coremanager_RefreshRate(arg) {
  if (!(arg instanceof core_manager_pb.RefreshRate)) {
    throw new Error('Expected argument of type coremanager.RefreshRate');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_coremanager_RefreshRate(buffer_arg) {
  return core_manager_pb.RefreshRate.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_data_SensorData(arg) {
  if (!(arg instanceof sensor_data_pb.SensorData)) {
    throw new Error('Expected argument of type data.SensorData');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_data_SensorData(buffer_arg) {
  return sensor_data_pb.SensorData.deserializeBinary(new Uint8Array(buffer_arg));
}


var CoreManagerService = exports.CoreManagerService = {
  // Notifies changes of configuration the connected clients
  listenCore: {
    path: '/coremanager.CoreManager/ListenCore',
    requestStream: false,
    responseStream: true,
    requestType: core_manager_pb.Empty,
    responseType: core_manager_pb.CoreChange,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_coremanager_CoreChange,
    responseDeserialize: deserialize_coremanager_CoreChange,
  },
  // Listen to data
  listenRawData: {
    path: '/coremanager.CoreManager/ListenRawData',
    requestStream: false,
    responseStream: true,
    requestType: core_manager_pb.Empty,
    responseType: sensor_data_pb.SensorData,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_data_SensorData,
    responseDeserialize: deserialize_data_SensorData,
  },
  listenInterpolatedData: {
    path: '/coremanager.CoreManager/ListenInterpolatedData',
    requestStream: false,
    responseStream: true,
    requestType: core_manager_pb.Empty,
    responseType: sensor_data_pb.SensorData,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_data_SensorData,
    responseDeserialize: deserialize_data_SensorData,
  },
  // Change data configurations
  changeDataRefreshRate: {
    path: '/coremanager.CoreManager/ChangeDataRefreshRate',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.RefreshRate,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_RefreshRate,
    requestDeserialize: deserialize_coremanager_RefreshRate,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  changeTimelapseInterpolation: {
    path: '/coremanager.CoreManager/ChangeTimelapseInterpolation',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.InterpolationLapse,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_InterpolationLapse,
    requestDeserialize: deserialize_coremanager_InterpolationLapse,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  // Trigger/reset cutoff
  triggerCutoff: {
    path: '/coremanager.CoreManager/TriggerCutoff',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Empty,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  resetCutoff: {
    path: '/coremanager.CoreManager/ResetCutoff',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Empty,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  // Changes cutoffs of derived measurement
  changePowertrainCutoff: {
    path: '/coremanager.CoreManager/ChangePowertrainCutoff',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.CutoffModifier,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_CutoffModifier,
    requestDeserialize: deserialize_coremanager_CutoffModifier,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  // Logger Configuration
  startRecording: {
    path: '/coremanager.CoreManager/StartRecording',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Empty,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  stopRecording: {
    path: '/coremanager.CoreManager/StopRecording',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Empty,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  changeUserDirectory: {
    path: '/coremanager.CoreManager/ChangeUserDirectory',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Path,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Path,
    requestDeserialize: deserialize_coremanager_Path,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  changeCacheDirectory: {
    path: '/coremanager.CoreManager/ChangeCacheDirectory',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Path,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Path,
    requestDeserialize: deserialize_coremanager_Path,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  takeSample: {
    path: '/coremanager.CoreManager/TakeSample',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Empty,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  generateCSV: {
    path: '/coremanager.CoreManager/GenerateCSV',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.CSVInfo,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_CSVInfo,
    requestDeserialize: deserialize_coremanager_CSVInfo,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  // Benchmarks Configuration
  saveBenchmark: {
    path: '/coremanager.CoreManager/SaveBenchmark',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Empty,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Empty,
    requestDeserialize: deserialize_coremanager_Empty,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  deleteBenchmark: {
    path: '/coremanager.CoreManager/DeleteBenchmark',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Benchmark,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Benchmark,
    requestDeserialize: deserialize_coremanager_Benchmark,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  renameBenchmark: {
    path: '/coremanager.CoreManager/RenameBenchmark',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Benchmark,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Benchmark,
    requestDeserialize: deserialize_coremanager_Benchmark,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
  changeBenchmarkMetadata: {
    path: '/coremanager.CoreManager/ChangeBenchmarkMetadata',
    requestStream: false,
    responseStream: false,
    requestType: core_manager_pb.Benchmark,
    responseType: core_manager_pb.Empty,
    requestSerialize: serialize_coremanager_Benchmark,
    requestDeserialize: deserialize_coremanager_Benchmark,
    responseSerialize: serialize_coremanager_Empty,
    responseDeserialize: deserialize_coremanager_Empty,
  },
};

exports.CoreManagerClient = grpc.makeGenericClientConstructor(CoreManagerService);
