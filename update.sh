#!/usr/bin/env bash

GIT_REPO=gitlab.com/rcbenchmark/rcbenchmark-proto_files

update_python() {
        # Update gRPC communicator => esc + parameter + handshake
	python -m grpc_tools.protoc -I ./proto --python_out=./python/communicator --grpc_python_out=./python/communicator communicator.proto
	protoc --python_out=./python/communicator -I ./proto esc.proto
	protoc --python_out=./python/communicator -I ./proto parameter.proto
	protoc --python_out=./python/communicator -I ./proto handshake.proto

        # Update gRPC observer => sensor_data
	python -m grpc_tools.protoc -I ./proto --python_out=./python/observer --grpc_python_out=./python/observer observer.proto
	protoc --python_out=./python/observer -I ./proto sensor_data.proto
	protoc --python_out=./python/observer -I ./proto handshake.proto
	protoc --python_out=./python/observer -I ./proto esc.proto

	# Update gRPC logger
	python -m grpc_tools.protoc -I ./proto --python_out=./python/logger --grpc_python_out=./python/logger logger.proto

	# Update gRPC core_manager
	python -m grpc_tools.protoc -I ./proto --python_out=./python/core_manager --grpc_python_out=./python/core_manager core_manager.proto
}

update_javascript() {
        # Update gRPC communicator => esc + parameter + handshake
        grpc_tools_node_protoc --grpc_out=generate_package_definition:./javascript/communicator --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` -I ./proto communicator.proto
	mv ./javascript/communicator/communicator_grpc_pb.js ./javascript/communicator/communicator_grpc_js.js
        protoc --js_out=import_style=commonjs:./javascript/communicator -I ./proto communicator.proto
        protoc --js_out=import_style=commonjs:./javascript/communicator -I ./proto esc.proto
        protoc --js_out=import_style=commonjs:./javascript/communicator -I ./proto parameter.proto
        protoc --js_out=import_style=commonjs:./javascript/communicator -I ./proto handshake.proto

        # Update gRPC observer => sensor_data
        grpc_tools_node_protoc --grpc_out=generate_package_definition:./javascript/observer --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` -I ./proto observer.proto
	mv ./javascript/observer/observer_grpc_pb.js ./javascript/observer/observer_grpc_js.js
        protoc --js_out=import_style=commonjs:./javascript/observer -I ./proto handshake.proto
        protoc --js_out=import_style=commonjs:./javascript/observer -I ./proto esc.proto
        protoc --js_out=import_style=commonjs:./javascript/observer -I ./proto observer.proto
        protoc --js_out=import_style=commonjs:./javascript/observer -I ./proto sensor_data.proto

	# Update gRPC logger
	grpc_tools_node_protoc --grpc_out=./javascript/logger --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` -I ./proto logger.proto
	mv ./javascript/logger/logger_grpc_pb.js ./javascript/logger/logger_grpc_native.js
	grpc_tools_node_protoc --grpc_out=generate_package_definition:./javascript/logger --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` -I ./proto logger.proto
	mv ./javascript/logger/logger_grpc_pb.js ./javascript/logger/logger_grpc_js.js
	protoc --js_out=import_style=commonjs:./javascript/logger -I ./proto logger.proto

	# Update gRPC core_manager
	grpc_tools_node_protoc --grpc_out=./javascript/core_manager --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` -I ./proto core_manager.proto
	mv ./javascript/core_manager/core_manager_grpc_pb.js ./javascript/core_manager/core_manager_grpc_native.js
	grpc_tools_node_protoc --grpc_out=generate_package_definition:./javascript/core_manager --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` -I ./proto core_manager.proto
	mv ./javascript/core_manager/core_manager_grpc_pb.js ./javascript/core_manager/core_manager_grpc_js.js
	protoc --js_out=import_style=commonjs:./javascript/core_manager -I ./proto core_manager.proto
	protoc --js_out=import_style=commonjs:./javascript/core_manager -I ./proto handshake.proto
	protoc --js_out=import_style=commonjs:./javascript/core_manager -I ./proto parameter.proto
	protoc --js_out=import_style=commonjs:./javascript/core_manager -I ./proto sensor_data.proto
}

update_golang() {
        protoc --go_out=plugins=grpc:./golang/communicator -I ./proto communicator.proto
        protoc --go_out=plugins=grpc:./golang/observer -I ./proto observer.proto
        protoc --go_out=plugins=grpc:./golang/app -I ./proto app.proto
        protoc --go_out=plugins=grpc:./golang/logger -I ./proto logger.proto
        protoc --go_out=plugins=grpc:./golang/core_manager -I ./proto core_manager.proto

        protoc --go_out=./golang/error -I ./proto error.proto
        protoc --go_out=./golang/esc -I ./proto esc.proto
        protoc --go_out=./golang/firmware -I ./proto firmware.proto
        protoc --go_out=./golang/handshake -I ./proto handshake.proto
        protoc --go_out=./golang/parameter -I ./proto parameter.proto
        protoc --go_out=./golang/sample -I ./proto sample.proto
        protoc --go_out=./golang/sensor_data -I ./proto sensor_data.proto

        # Update imports
        sed -i "s|import (|import (\n\
        . \"$GIT_REPO/golang/error\"\n\
        . \"$GIT_REPO/golang/handshake\"\n\
        . \"$GIT_REPO/golang/parameter\"\n\
        . \"$GIT_REPO/golang/sensor_data\"\n\
        |" ./golang/firmware/firmware.pb.go

       	sed -i "s|import (|import (\n\
        . \"$GIT_REPO/golang/handshake\"\n\
        . \"$GIT_REPO/golang/parameter\"\n\
        . \"$GIT_REPO/golang/sensor_data\"\n\
        |" ./golang/core_manager/core_manager.pb.go
         
	 
	sed -i "s|import (|import (\n\
        . \"$GIT_REPO/golang/sensor_data\"\n\
        . \"$GIT_REPO/golang/parameter\"\n\
        |" ./golang/sample/sample.pb.go
         
	sed -i "s|import (|import (\n\
        . \"$GIT_REPO/golang/esc\"\n\
        |" ./golang/handshake/handshake.pb.go
         
	sed -i "s|import (|import (\n\
        . \"$GIT_REPO/golang/esc\"\n\
        . \"$GIT_REPO/golang/parameter\"\n\
        |" ./golang/communicator/communicator.pb.go
        
	 sed -i "s|import (|import (\n\
        . \"$GIT_REPO/golang/sensor_data\"\n\
        . \"$GIT_REPO/golang/parameter\"\n\
        . \"$GIT_REPO/golang/handshake\"\n\
        |" ./golang/observer/observer.pb.go
        
	sed -i "s|import (|import (\n\
        . \"$GIT_REPO/golang/parameter\"\n\
        . \"$GIT_REPO/golang/sensor_data\"\n\
        |" ./golang/error/error.pb.go

        sed -i "s|import (|import (\n\
        . \"$GIT_REPO/golang/esc\"\n\
        |" ./golang/parameter/parameter.pb.go
}

update_python
update_javascript
update_golang
