// GENERATED CODE -- DO NOT EDIT!

'use strict';
var communicator_pb = require('./communicator_pb.js');
var esc_pb = require('./esc_pb.js');
var parameter_pb = require('./parameter_pb.js');

function serialize_communicatorproto_ESCModifier(arg) {
  if (!(arg instanceof communicator_pb.ESCModifier)) {
    throw new Error('Expected argument of type communicatorproto.ESCModifier');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_communicatorproto_ESCModifier(buffer_arg) {
  return communicator_pb.ESCModifier.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_communicatorproto_Empty(arg) {
  if (!(arg instanceof communicator_pb.Empty)) {
    throw new Error('Expected argument of type communicatorproto.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_communicatorproto_Empty(buffer_arg) {
  return communicator_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_communicatorproto_FirmwareFlasher(arg) {
  if (!(arg instanceof communicator_pb.FirmwareFlasher)) {
    throw new Error('Expected argument of type communicatorproto.FirmwareFlasher');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_communicatorproto_FirmwareFlasher(buffer_arg) {
  return communicator_pb.FirmwareFlasher.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_communicatorproto_PacketSender(arg) {
  if (!(arg instanceof communicator_pb.PacketSender)) {
    throw new Error('Expected argument of type communicatorproto.PacketSender');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_communicatorproto_PacketSender(buffer_arg) {
  return communicator_pb.PacketSender.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_communicatorproto_ParamModifier(arg) {
  if (!(arg instanceof communicator_pb.ParamModifier)) {
    throw new Error('Expected argument of type communicatorproto.ParamModifier');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_communicatorproto_ParamModifier(buffer_arg) {
  return communicator_pb.ParamModifier.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_communicatorproto_Port(arg) {
  if (!(arg instanceof communicator_pb.Port)) {
    throw new Error('Expected argument of type communicatorproto.Port');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_communicatorproto_Port(buffer_arg) {
  return communicator_pb.Port.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_communicatorproto_Ports(arg) {
  if (!(arg instanceof communicator_pb.Ports)) {
    throw new Error('Expected argument of type communicatorproto.Ports');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_communicatorproto_Ports(buffer_arg) {
  return communicator_pb.Ports.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_communicatorproto_Protocol(arg) {
  if (!(arg instanceof communicator_pb.Protocol)) {
    throw new Error('Expected argument of type communicatorproto.Protocol');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_communicatorproto_Protocol(buffer_arg) {
  return communicator_pb.Protocol.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_parameter_Parameter(arg) {
  if (!(arg instanceof parameter_pb.Parameter)) {
    throw new Error('Expected argument of type parameter.Parameter');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_parameter_Parameter(buffer_arg) {
  return parameter_pb.Parameter.deserializeBinary(new Uint8Array(buffer_arg));
}


var CommunicatorService = exports['communicatorproto.Communicator'] = {
  // Returns a list of port available for the given protocol (if supported)
  getAvailablePorts: {
    path: '/communicatorproto.Communicator/GetAvailablePorts',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.Protocol,
    responseType: communicator_pb.Ports,
    requestSerialize: serialize_communicatorproto_Protocol,
    requestDeserialize: deserialize_communicatorproto_Protocol,
    responseSerialize: serialize_communicatorproto_Ports,
    responseDeserialize: deserialize_communicatorproto_Ports,
  },
  // Returns the list of connected ports
  getConnectedPorts: {
    path: '/communicatorproto.Communicator/GetConnectedPorts',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.Empty,
    responseType: communicator_pb.Ports,
    requestSerialize: serialize_communicatorproto_Empty,
    requestDeserialize: deserialize_communicatorproto_Empty,
    responseSerialize: serialize_communicatorproto_Ports,
    responseDeserialize: deserialize_communicatorproto_Ports,
  },
  // Connect to the given port
  connectTo: {
    path: '/communicatorproto.Communicator/ConnectTo',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.Port,
    responseType: communicator_pb.Empty,
    requestSerialize: serialize_communicatorproto_Port,
    requestDeserialize: deserialize_communicatorproto_Port,
    responseSerialize: serialize_communicatorproto_Empty,
    responseDeserialize: deserialize_communicatorproto_Empty,
  },
  // Disconnect from the given port
  disconnectFrom: {
    path: '/communicatorproto.Communicator/DisconnectFrom',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.Port,
    responseType: communicator_pb.Empty,
    requestSerialize: serialize_communicatorproto_Port,
    requestDeserialize: deserialize_communicatorproto_Port,
    responseSerialize: serialize_communicatorproto_Empty,
    responseDeserialize: deserialize_communicatorproto_Empty,
  },
  // Parameters methods 
  //
  // Change the given parameter on the given board
  changeParameter: {
    path: '/communicatorproto.Communicator/ChangeParameter',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.ParamModifier,
    responseType: communicator_pb.Empty,
    requestSerialize: serialize_communicatorproto_ParamModifier,
    requestDeserialize: deserialize_communicatorproto_ParamModifier,
    responseSerialize: serialize_communicatorproto_Empty,
    responseDeserialize: deserialize_communicatorproto_Empty,
  },
  // Returns all the parameters of a board
  fetchBoardsParams: {
    path: '/communicatorproto.Communicator/FetchBoardsParams',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.Empty,
    responseType: parameter_pb.Parameter,
    requestSerialize: serialize_communicatorproto_Empty,
    requestDeserialize: deserialize_communicatorproto_Empty,
    responseSerialize: serialize_parameter_Parameter,
    responseDeserialize: deserialize_parameter_Parameter,
  },
  // ESC Controller methods
  changeESC: {
    path: '/communicatorproto.Communicator/ChangeESC',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.ESCModifier,
    responseType: communicator_pb.Empty,
    requestSerialize: serialize_communicatorproto_ESCModifier,
    requestDeserialize: deserialize_communicatorproto_ESCModifier,
    responseSerialize: serialize_communicatorproto_Empty,
    responseDeserialize: deserialize_communicatorproto_Empty,
  },
  // Send byte packet to a board
  sendPacket: {
    path: '/communicatorproto.Communicator/SendPacket',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.PacketSender,
    responseType: communicator_pb.Empty,
    requestSerialize: serialize_communicatorproto_PacketSender,
    requestDeserialize: deserialize_communicatorproto_PacketSender,
    responseSerialize: serialize_communicatorproto_Empty,
    responseDeserialize: deserialize_communicatorproto_Empty,
  },
  // Flash Firmware 
  flashFirmware: {
    path: '/communicatorproto.Communicator/FlashFirmware',
    requestStream: false,
    responseStream: false,
    requestType: communicator_pb.FirmwareFlasher,
    responseType: communicator_pb.Empty,
    requestSerialize: serialize_communicatorproto_FirmwareFlasher,
    requestDeserialize: deserialize_communicatorproto_FirmwareFlasher,
    responseSerialize: serialize_communicatorproto_Empty,
    responseDeserialize: deserialize_communicatorproto_Empty,
  },
};

// Connection methods 
